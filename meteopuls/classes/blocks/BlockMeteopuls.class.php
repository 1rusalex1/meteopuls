<?php
/*-------------------------------------------------------
*
*   LiveStreet Engine Social Networking
*   Copyright © 2008 Mzhelskiy Maxim
*
*--------------------------------------------------------
*
*   Official site: www.livestreet.ru
*   Contact e-mail: rus.engine@gmail.com
*
*   GNU General Public License, version 2:
*   http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*
---------------------------------------------------------
*/

/**
 * Обработка блока с рейтингом блогов
 *
 * @package blocks
 * @since 1.0
 */
class PluginMeteopuls_BlockMeteopuls extends Block {
	/**
	 * Запуск обработки
	 */
	public function Exec() {
		/**
		 * Получаем данные по погоде.
		 */
        $sCityName=$this->PluginMeteopuls_Geodetection_GetValue();
        $this->Logger_Debug('cityname='.print_r($sCityName,true));
        if ($sCityName) {
            $oCity=$this->PluginMeteopuls_Geodetection_GetCityByName($sCityName);
            $sCityId=$oCity->getCityId();
            if (isset($sCityId) and ($sCityId)) {}
            else $sCityId=27612;
      $aOutFact=$this->PluginMeteopuls_Meteopuls_GetFactWeather ($sCityId);
		if ($aOutFact) {
            $this->Viewer_Assign('aOutFact',$aOutFact);
		}
        else {$this->Logger_Debug('Нет данных от Яндекса, поэтому блок не выводится.');}
	}
    }
}
?>