<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Rusalex
 * Date: 27.02.14
 * Time: 4:36
 * To change this template use File | Settings | File Templates.
 */
class PluginMeteopuls_ModuleGeodetection_EntityCityip extends Entity
{
    public function getCityId() {
        return $this->_getDataOne('city_id');
    }

    public function getCityName() {
        return $this->_getDataOne('city_name');
    }

}