<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Rusalex
 * Date: 07.03.14
 * Time: 4:21
 * To change this template use File | Settings | File Templates.
 */

class PluginMeteopuls_ModuleGeodetection_MapperCityip extends Mapper {


    /**
     *Получаем объект сущности по названию города.
    */

public function GetCityByName($sName) {
    $sql = "SELECT * FROM ".Config::Get('plugin.meteopuls.table.city_ip')." WHERE city_name = ? ";
    if ($aRow=$this->oDb->selectRow($sql,$sName)) {
        return Engine::GetEntity('PluginMeteopuls_ModuleGeodetection_EntityCityip',$aRow);
    }
    return null;
}

}