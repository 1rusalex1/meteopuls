<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Rusalex
 * Date: 23.02.14
 * Time: 18:37
 * To change this template use File | Settings | File Templates.
 */
class PluginMeteopuls_ModuleGeodetection extends Module
{

    protected $charset='UTF-8';
    protected $oMapper;

    public function Init()
    {
        $this->Logger_Debug('Модуль PluginMeteopuls_ModuleGeodetection инициализирован');
        $this->oMapper=Engine::GetMapper(__CLASS__, 'Cityip');
    }


    public function GetCityByName ($sCityName) {
        return $this->oMapper->GetCityByName($sCityName);
    }


    /**
     * функция возвращет конкретное значение из полученного массива данных по ip
     * @param string - ключ массива. Если интересует конкретное значение.
     * Ключ может быть равным 'inetnum', 'country', 'city', 'region', 'district', 'lat', 'lng'
     * @param bolean - устанавливаем хранить данные в куки или нет
     * Если true, то в куки будут записаны данные по ip и повторные запросы на ipgeobase происходить не будут.
     * Если false, то данные постоянно будут запрашиваться с ipgeobase
     * @return array OR string - дополнительно читайте комментарии внутри функции.
     */
  public function GetValue($key = 'city', $cookie = true)
    {
        $ip=$this->GetIp();
        $this->Logger_Debug(print_r('ip='.$ip,true));
        $key_array = array('inetnum', 'country', 'city', 'region', 'district', 'lat', 'lng');
        if(!in_array($key, $key_array))
            $key = false;

        // если используем куки и параметр уже получен, то достаем и возвращаем данные из куки
        if($cookie && isset($_COOKIE['geobase']))
        {
            $data = unserialize($_COOKIE['geobase']);
        }
        else
        {
            $data = $this->GetGeobaseData($ip);
            if (!$data) {return false;}
            setcookie('geobase', serialize($data), time()+3600*24*365*2); //устанавливаем куки на 2 года
        }
        if($key)
            return $data[$key]; // если указан ключ, возвращаем строку с нужными данными
        else
            return $data; // иначе возвращаем массив со всеми данными
    }

    /**
     * функция получает данные по ip.
     * @return array - возвращает массив с данными
     */
   protected function GetGeobaseData($ip)
    {
        // получаем данные по ip
        $link = 'ipgeobase.ru:7020/geo?ip='.$ip;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $link);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,3);
        $string = curl_exec($ch);
        if (!$string) {return false;}

        // если указана кодировка отличная от windows-1251, изменяем кодировку
        if($this->charset)
            $string = iconv('windows-1251', $this->charset, $string);

        $data = $this->ParseString($string);

        $this->Logger_Debug(print_r('Geobasedata='.$data,true));
        return $data;
    }

    /**
     * функция парсит полученные в XML данные в случае, если на сервере не установлено расширение Simplexml
     * @return array - возвращает массив с данными
     */

   private function ParseString($string)
    {
        $pa['inetnum'] = '#<inetnum>(.*)</inetnum>#is';
        $pa['country'] = '#<country>(.*)</country>#is';
        $pa['city'] = '#<city>(.*)</city>#is';
        $pa['region'] = '#<region>(.*)</region>#is';
        $pa['district'] = '#<district>(.*)</district>#is';
        $pa['lat'] = '#<lat>(.*)</lat>#is';
        $pa['lng'] = '#<lng>(.*)</lng>#is';
        $data = array();
        foreach($pa as $key => $pattern)
        {
            if(preg_match($pattern, $string, $out))
            {
                $data[$key] = trim($out[1]);
            }
        }
        return $data;
    }

    /**
     * функция определяет ip адрес по глобальному массиву $_SERVER
     * ip адреса проверяются начиная с приоритетного, для определения возможного использования прокси
     * @return ip-адрес
     * для наших целей определения города подойдет и remote_addr, так как у нас необходимости привязывать
     * какую-либо секретную информацию к ip.
     */
   public function GetIp()
    {
        $ip = false;
     //   if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
     //       $ipa[] = trim(strtok($_SERVER['HTTP_X_FORWARDED_FOR'], ',')); // для определения ip компьютеров, которые подключаются через прокси, это

     //   if (isset($_SERVER['HTTP_CLIENT_IP']))     // этот адрес порой передается в заголовке вместе или вместо HTTP_X_FORWARDED_FOR
     //       $ipa[] = $_SERVER['HTTP_CLIENT_IP'];

        if (isset($_SERVER['REMOTE_ADDR']))   //это настоящий внешний уникальный адрес прокси или компьютера, подключенного к интернету.
            $ipa[] = $_SERVER['REMOTE_ADDR'];

     //  if (isset($_SERVER['HTTP_X_REAL_IP']))  // этот адрес также приходит в заголовке и может быть ошибочным или поддельным.
     //       $ipa[] = $_SERVER['HTTP_X_REAL_IP'];

        // проверяем ip-адреса на валидность начиная с приоритетного.
        foreach($ipa as $ips)
        {
            //  если ip валидный обрываем цикл, назначаем ip адрес и возвращаем его
            if($this->IsValidIp($ips))
            {
                $ip = $ips;
                break;
            }
        }

        // УДАЛИТЬ ПОСЛЕ ОТЛАДКИ НА ЛОКАЛЬНОМ СЕРВЕРЕ!!!
        //------------------------------------------------

        $ip='188.162.65.58';

        //-------------------------------------------------

    return '188.162.65.58';


    }

    /**
     * функция для проверки валидности ip адреса
     * @param ip адрес в формате 1.2.3.4
     * @return boolean : true - если ip валидный, иначе false
     */
    protected function IsValidIp($ip=null)
    {
        if(preg_match("#^([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})$#", $ip))
            return true; // если ip-адрес попадает под регулярное выражение, возвращаем true

        return false; // иначе возвращаем false
    }


}
?>